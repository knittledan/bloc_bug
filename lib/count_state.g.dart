// GENERATED CODE - DO NOT MODIFY BY HAND

part of count_state;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$CountState extends CountState {
  @override
  final String error;
  @override
  final int count;

  factory _$CountState([void Function(CountStateBuilder) updates]) =>
      (new CountStateBuilder()..update(updates)).build();

  _$CountState._({this.error, this.count}) : super._() {
    if (error == null) {
      throw new BuiltValueNullFieldError('CountState', 'error');
    }
    if (count == null) {
      throw new BuiltValueNullFieldError('CountState', 'count');
    }
  }

  @override
  CountState rebuild(void Function(CountStateBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  CountStateBuilder toBuilder() => new CountStateBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is CountState && error == other.error && count == other.count;
  }

  @override
  int get hashCode {
    return $jf($jc($jc(0, error.hashCode), count.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('CountState')
          ..add('error', error)
          ..add('count', count))
        .toString();
  }
}

class CountStateBuilder implements Builder<CountState, CountStateBuilder> {
  _$CountState _$v;

  String _error;
  String get error => _$this._error;
  set error(String error) => _$this._error = error;

  int _count;
  int get count => _$this._count;
  set count(int count) => _$this._count = count;

  CountStateBuilder();

  CountStateBuilder get _$this {
    if (_$v != null) {
      _error = _$v.error;
      _count = _$v.count;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(CountState other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$CountState;
  }

  @override
  void update(void Function(CountStateBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$CountState build() {
    final _$result = _$v ?? new _$CountState._(error: error, count: count);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
