library count_state;

import 'package:built_value/built_value.dart';
part 'count_state.g.dart';

abstract class CountState implements Built<CountState, CountStateBuilder> {
  String get error;
  int get count;

  CountState._();

  factory CountState([updates(CountStateBuilder b)]) = _$CountState;

  factory CountState.initial() {
    return CountState((b) => b
      ..error = ''
      ..count = 0);
  }

  factory CountState.success(int newCount) {
    return CountState((b) => b
      ..error = ''
      ..count = newCount);
  }

  factory CountState.failure(String error) {
    return CountState((b) => b
      ..error = error
      ..count = 0);
  }
}