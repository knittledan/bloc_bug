// GENERATED CODE - DO NOT MODIFY BY HAND

part of counter_event;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$CounterInitiated extends CounterInitiated {
  @override
  final String query;

  factory _$CounterInitiated(
          [void Function(CounterInitiatedBuilder) updates]) =>
      (new CounterInitiatedBuilder()..update(updates)).build();

  _$CounterInitiated._({this.query}) : super._() {
    if (query == null) {
      throw new BuiltValueNullFieldError('CounterInitiated', 'query');
    }
  }

  @override
  CounterInitiated rebuild(void Function(CounterInitiatedBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  CounterInitiatedBuilder toBuilder() =>
      new CounterInitiatedBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is CounterInitiated && query == other.query;
  }

  @override
  int get hashCode {
    return $jf($jc(0, query.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('CounterInitiated')
          ..add('query', query))
        .toString();
  }
}

class CounterInitiatedBuilder
    implements Builder<CounterInitiated, CounterInitiatedBuilder> {
  _$CounterInitiated _$v;

  String _query;
  String get query => _$this._query;
  set query(String query) => _$this._query = query;

  CounterInitiatedBuilder();

  CounterInitiatedBuilder get _$this {
    if (_$v != null) {
      _query = _$v.query;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(CounterInitiated other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$CounterInitiated;
  }

  @override
  void update(void Function(CounterInitiatedBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$CounterInitiated build() {
    final _$result = _$v ?? new _$CounterInitiated._(query: query);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
