library counter_event;

import 'package:built_value/built_value.dart';

part 'counter_event.g.dart';


abstract class CounterEvent {}

abstract class CounterInitiated extends CounterEvent implements Built<CounterInitiated, CounterInitiatedBuilder> {
  String get query;

  CounterInitiated._();

  factory CounterInitiated([updates(CounterInitiatedBuilder b)]) = _$CounterInitiated;
}


class ExceptionEvent extends CounterEvent {}

class IncrementEvent extends CounterEvent {}

class DecrementEvent extends CounterEvent {}