library fake_repository;

import 'package:bug_code/exception.dart';

class FakeRepository {
  int _currentCount = 0;

  Future<int> increment() async {
    final newCount = _currentCount + 1;
    _cacheValues(newCount: newCount);
    return newCount;
  }

  Future<int> deIncrement() async {
    final newCount = _currentCount - 1;
    _cacheValues(newCount: newCount);
    return newCount;
  }

  Future<int> throwException() async {
    throw BugException();
  }

  void _cacheValues({newCount}) {
    _currentCount = newCount;
  }
}