import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:bug_code/count_state.dart';
import 'package:bug_code/counter_event.dart';
import 'package:bug_code/exception.dart';
import 'package:bug_code/fake_repository.dart';


class CounterBloc extends Bloc<CounterEvent, CountState> {
  final FakeRepository _fakeRepository = FakeRepository();

  void increment() {
    dispatch(IncrementEvent());
  }

  void deIncrement() {
    dispatch(DecrementEvent());
  }

  void bugException() {
    dispatch(ExceptionEvent());
  }

  @override
  CountState get initialState => CountState.initial();

  @override
  Stream<CountState> mapEventToState(CounterEvent event) async* {
    if (event is CounterInitiated) {
      yield CountState.initial();
    } else if (event is DecrementEvent) {
      int newValue = await _fakeRepository.deIncrement();
      yield CountState.success(newValue);
    } else if (event is IncrementEvent) {
      int newValue = await _fakeRepository.increment();
      yield CountState.success(newValue);
    } else {
      try {
        int newValue = await _fakeRepository.throwException();
//        int newValue = await _fakeRepository.throwException().catchError((e) {
//          // Using catch error will allow dispatch to continue to work.
//          print(e);
//        });
      } on BugException catch(e) {
        print("Exception being thrown");
        yield CountState.failure(e.message);
      }
    }
  }
}